import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CanonicalService } from './services/canonical.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/views/home-page/home-page.component';
import { AboutUsPageComponent } from './pages/views/about-us-page/about-us-page.component';
import { CausesPageComponent } from './pages/views/causes-page/causes-page.component';
import { EventsPageComponent } from './pages/views/events-page/events-page.component';
import { BlogPageComponent } from './pages/views/blog-page/blog-page.component';
import { ContactPageComponent } from './pages/views/contact-page/contact-page.component';
import { HeaderComponent } from './pages/shared/header/header.component';
import { FooterComponent } from './pages/shared/footer/footer.component';
import { AboutIntroComponent } from './pages/views/about-us-page/about-intro/about-intro.component';
import { AboutTeamComponent } from './pages/views/about-us-page/about-team/about-team.component';
import { VolunteerComponent } from './pages/views/volunteer/volunteer.component';
import { MajorCausesComponent } from './pages/views/causes-page/major-causes/major-causes.component';
import { FeatureCausesComponent } from './pages/views/causes-page/feature-causes/feature-causes.component';
import { SubscribeComponent } from './pages/views/subscribe/subscribe.component';
import { UpcomingEventsComponent } from './pages/views/events-page/upcoming-events/upcoming-events.component';
import { BlogCategoriesComponent } from './pages/views/blog-page/blog-categories/blog-categories.component';
import { BlogContentComponent } from './pages/views/blog-page/blog-content/blog-content.component';
import { BlogWidgetComponent } from './pages/views/blog-page/blog-widget/blog-widget.component';
import { MapWidgetComponent } from './pages/views/contact-page/map-widget/map-widget.component';
import { ContactFormComponent } from './pages/views/contact-page/contact-form/contact-form.component';
import { PageNotFoundComponent } from './pages/views/page-not-found/page-not-found.component';
import { NewStoryComponent } from './pages/views/blog-page/new-story/new-story.component';
import { EventInfoComponent } from './pages/views/events-page/event-info/event-info.component';
import { BlogInfoComponent } from './pages/views/blog-page/blog-info/blog-info.component';
import { BlogCommentsComponent } from './pages/views/blog-page/blog-comments/blog-comments.component';
import { BlogPostComponent } from './pages/views/blog-page/blog-post/blog-post.component';
import { DataService } from './services/data.service';
import { SeoService } from './services/seo.service';
import { GallerypageComponent } from './pages/views/gallerypage/gallerypage.component';
import { GalleryComponent } from './pages/views/gallerypage/gallery/gallery.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomePageComponent,
    AboutUsPageComponent,
    CausesPageComponent,
    EventsPageComponent,
    BlogPageComponent,
    ContactPageComponent,
    AboutIntroComponent,
    AboutTeamComponent,
    VolunteerComponent,
    MajorCausesComponent,
    FeatureCausesComponent,
    SubscribeComponent,
    UpcomingEventsComponent,
    BlogCategoriesComponent,
    BlogContentComponent,
    BlogWidgetComponent,
    MapWidgetComponent,
    ContactFormComponent,
    PageNotFoundComponent,
    NewStoryComponent,
    EventInfoComponent,
    BlogInfoComponent,
    BlogCommentsComponent,
    BlogPostComponent,
    GallerypageComponent,
    GalleryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [DataService, SeoService, CanonicalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
