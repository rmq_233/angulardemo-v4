import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CanonicalService } from "./services/canonical.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  constructor (
    private canonicalService: CanonicalService,
    private route:Router
    ) {}

  ngOnInit() {
    this.canonicalService.setCanonicalURL();
  }
}
