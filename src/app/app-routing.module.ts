import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './pages/views/home-page/home-page.component';
import { AboutUsPageComponent } from './pages/views/about-us-page/about-us-page.component';
import { CausesPageComponent } from './pages/views/causes-page/causes-page.component';
import { EventsPageComponent } from './pages/views/events-page/events-page.component';
import { BlogPageComponent } from './pages/views/blog-page/blog-page.component';
import { ContactPageComponent } from './pages/views/contact-page/contact-page.component';
import { PageNotFoundComponent } from "./pages/views/page-not-found/page-not-found.component";
import { EventInfoComponent } from "./pages/views/events-page/event-info/event-info.component";
import { BlogInfoComponent } from "./pages/views/blog-page/blog-info/blog-info.component";
import { GallerypageComponent } from './pages/views/gallerypage/gallerypage.component';



const routes: Routes = [
	{
		path: "",
		component: HomePageComponent,
	},
	{
		path: "home",
		component: HomePageComponent,
	},
	{
		path: "about_us",
		component: AboutUsPageComponent,
	},
	{
		path: "causes",
		component: CausesPageComponent,
	},
	// {
	//   path: "events",
	//   children: [
	//     {
	//       path: "", // child route path
	//       component: EventsPageComponent, // child route component that the router renders
	//     },
	//     {
	//       path: "event_info", // child route path
	//       component: EventInfoComponent, // child route component that the router renders
	//     },
	//   ],
	// },
	// {
	//   path: "blog",
	//   children: [
	//     {
	//       path: "", // child route path
	//       component: BlogPageComponent, // child route component that the router renders
	//     },
	//     {
	//       path: "blog_info", // child route path
	//       component: BlogInfoComponent, // child route component that the router renders
	//     },
	//   ],
	// },
	{
		path: "gallery",
		component: GallerypageComponent,
	},
	{
		path: "contact_us",
		component: ContactPageComponent,
	},
	{
		path: "**",
		component: PageNotFoundComponent,
	},
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes
      // { enableTracing: true } // <-- debugging purposes only
    ),
    // other imports here
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
