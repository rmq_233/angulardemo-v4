import { Component, OnInit } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";
import * as $ from "jquery";

@Component({
	selector: "app-about-us-page",
	templateUrl: "./about-us-page.component.html",
	styleUrls: ["./about-us-page.component.css"],
})
export class AboutUsPageComponent implements OnInit {
	constructor(private titleService: Title, private meta: Meta) {
		this.titleService.setTitle("About Us | Grace To Grace Charity Foundation");
		this.meta.updateTag({
			name: "description",
			content:
				"My Name Is George Arthur Baiden. I Am The Founder Of Grace To Grace Charity Foundation, We Cater For Almost 255 Orphans And Needy But Brilliant Children At The Ages Of 2 - 15 Years, At Gomoa Fetteh Kakraba,   In The Central Region Of Ghana.",
		});
		this.meta.updateTag({ name: "keywords", content: "" });
		this.meta.updateTag({ name: "author", content: "RMQ Ceative" });
	}

	aboutBanner = {
		mainText: "About Us",
		subText: "Who we are, What we do & Why we are doing it.",
	};

	asideA = {
		title: "Vision",
		image: "/assets/img/about/vision.jpg",
		text: "To provide a unique humanitarian outreach and support services, to	underprivileged children and communities through the provision of food, clothes, healthcare and quality educational facilities and also extend its horizon outside ghana and strategically position them to be able to impact the world.",
	};

	asideB = {
		title: "Mission",
		image: "/assets/img/about/mission.jpg",
		text: "To work with organizations and communities to provide sustainable quality healthcare for underprivileged children in the hinterlands, improving the standard of education in deprived communities, initiates development projects with support from stakeholders and organizations, building the capacity of the underserved and the disadvantaged children and the communities, mobilizing resources for the development of improve the livelihood of deprived children and its communities.",
  };

	ngOnInit() {}
}
