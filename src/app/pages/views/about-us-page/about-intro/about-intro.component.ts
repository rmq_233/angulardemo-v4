import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-about-intro",
  templateUrl: "./about-intro.component.html",
  styleUrls: ["./about-intro.component.css"],
})
export class AboutIntroComponent implements OnInit {
  constructor() {}

  aboutIntro = {
    title: "We Are The Arthur Baidens",
    intro:
      "We cater for almost 255 orphans and needy but brilliant children at the ages of 2 - 15 years, at gomoa fetteh kakraba, in the Central Region of Ghana - West Africa.",
    text:
      "Grace to Grace Charity Foundation was established in 2014 by george arthur, a ghanaian. the ngo is registered with the registrar generals department. the project is focused on orphans, street kids and needy children from deprived communities within the ages of 2 to 14 years. the project feeds, clothes and educates about 264 orphans and needy children. the project is blessed with a school called heavenly grace educational complex from crèche to junior high school, which is also registered with the registrar generals department and ghana education service with 16 teaching staffs and 2 cleaners.",
  };

  ngOnInit(): void {}
}
