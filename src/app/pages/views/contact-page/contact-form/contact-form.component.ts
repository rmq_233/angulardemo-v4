import { Component, OnInit } from '@angular/core';

@Component({
	selector: "app-contact-form",
	templateUrl: "./contact-form.component.html",
	styleUrls: ["./contact-form.component.css"],
})
export class ContactFormComponent implements OnInit {
	constructor() {}

	contactList: any[] = [
		{
			title: "High Tension",
			text: "Gomoa Fetteh Kakraba, Central Region - Ghana",
			icon: "lnr-home",
		},
		{
			title: "Speak with us anytime.",
			text: "Mon - Sun | (+233) 54 769 6997 / (+233) 54 297 3008",
			icon: "lnr-phone-handset",
		},
		{
			title: "Email Us",
			text: "info@gracefoundus.org",
			icon: "lnr-envelope",
		},
	];

	ngOnInit() {}
}
