import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: "app-contact-page",
  templateUrl: "./contact-page.component.html",
  styleUrls: ["./contact-page.component.css"],
})
export class ContactPageComponent implements OnInit {
  constructor(private titleService: Title, private meta: Meta) {
    this.titleService.setTitle(
      "Contact Us | Grace To Grace Charity Foundation"
    );
    this.meta.updateTag({
      name: "description",
      content: "",
    });
    this.meta.updateTag({ name: "keywords", content: "" });
    this.meta.updateTag({ name: "author", content: "RMQ Ceative" });
  }

  contactBanner = {
    mainText: "Contact Us",
    subText:
      "For more information about Grace To Grace Charity Foundation Causes,  becoming a sponsor, volunteering, fundraising and/or donations please contact.",
  };

  ngOnInit(): void {}
}
