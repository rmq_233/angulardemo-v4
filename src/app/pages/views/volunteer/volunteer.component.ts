import { Component, OnInit } from '@angular/core';

@Component({
	selector: "app-volunteer",
	templateUrl: "./volunteer.component.html",
	styleUrls: ["./volunteer.component.css"],
})
export class VolunteerComponent implements OnInit {
	constructor() {}

	volunteerInfo = {
		title: "Give A Hand",
		subTitle:
			"Your donations means we can bring hope to our children. Together we are fighting, better and harder than we ever have before. Thank you for your support.",
		text1: "0591324546",
		text2: "Grace To Grace Charity Foundation",
		image: "/assets/img/volunteer/mtn.jpeg",
	};

	ngOnInit(): void {}
}
