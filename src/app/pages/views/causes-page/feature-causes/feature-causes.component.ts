import { Component, OnInit } from "@angular/core";

@Component({
	selector: "app-feature-causes",
	templateUrl: "./feature-causes.component.html",
	styleUrls: ["./feature-causes.component.css"],
})
export class FeatureCausesComponent implements OnInit {
	constructor() {}

	// Page Title
	page = {
		title: "Featured Causes",
		subText: "We have a few pledges we may ask you to support",
	};

	featuredCauses: any[] = [
		{
			id: "1",
			title: "Improving ICT Studies",
			text:
				"We are in need of computers and printers for our soon to be computer labs. Any used or old ones can be donated to us.",
			image: "assets/img/features/ictlab.jpg",
			goal: "10,000 GHs",
			raised: "1,350 GHs",
			donors: "6",
			buttonName: "Support",
			buttonLink: "/causes",
		},
		{
			id: "2",
			title: "New and Improved Library",
			text:
				"Any book that helps a child to form a habit of reading, to make reading one of his deep and continuing needs, is good for him. ",
			image: "assets/img/features/library.jpg",
			goal: "1,000,000 Books",
			raised: "500 Books",
			donors: "140",
			buttonName: "Support",
			buttonLink: "/causes",
		},
		{
			id: "3",
			title: "Sustainable Portable Water",
			text:
				"One of our many challenges is sustainable water for the school and foundation. ",
			image: "assets/img/features/water.jpg",
			goal: "3 Water Boreholes",
			raised: "0",
			donors: "0",
			buttonName: "Support",
			buttonLink: "/causes",
		},
	];

	ngOnInit(): void {}
}
