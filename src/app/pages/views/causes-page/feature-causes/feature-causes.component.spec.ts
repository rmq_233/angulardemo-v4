import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureCausesComponent } from './feature-causes.component';

describe('FeatureCausesComponent', () => {
  let component: FeatureCausesComponent;
  let fixture: ComponentFixture<FeatureCausesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeatureCausesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeatureCausesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
