import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: "app-causes-page",
  templateUrl: "./causes-page.component.html",
  styleUrls: ["./causes-page.component.css"],
})
export class CausesPageComponent implements OnInit {
  constructor(private titleService: Title, private meta: Meta) {
    this.titleService.setTitle(
      "Our Causes | Grace To Grace Charity Foundation"
    );
    this.meta.updateTag({
      name: "description",
      content: "",
    });
    this.meta.updateTag({ name: "keywords", content: "" });
    this.meta.updateTag({ name: "author", content: "RMQ Ceative" });
  }

  causesBanner = {
    mainText: "Causes",
    subText: "What we do, How we are doing it & How you can help.",
  };

  ngOnInit(): void {}
}
