import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CausesPageComponent } from './causes-page.component';

describe('CausesPageComponent', () => {
  let component: CausesPageComponent;
  let fixture: ComponentFixture<CausesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CausesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CausesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
