import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MajorCausesComponent } from './major-causes.component';

describe('MajorCausesComponent', () => {
  let component: MajorCausesComponent;
  let fixture: ComponentFixture<MajorCausesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MajorCausesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajorCausesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
