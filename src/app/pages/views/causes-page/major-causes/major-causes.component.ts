import { Component, OnInit } from '@angular/core';

@Component({
	selector: "app-major-causes",
	templateUrl: "./major-causes.component.html",
	styleUrls: ["./major-causes.component.css"],
})
export class MajorCausesComponent implements OnInit {
	constructor() {}

	// SEO
	causeTitle = {
		title: "Our Major Causes",
		subText:
			"Our goals and vision turned to causes we fight each day to support",
	};

	majorCauses: any[] = [
		{
			title: "Funding",
			image: "assets/img/causes/c1.png",
			text:
				"To solicit for funds and scholarships for needy but brilliant students from primary school through to the universities, to provide them with the appropriate skills and trainings.",
		},
		{
			title: "Welfare & Support",
			image: "assets/img/causes/c2.png",
			text:
				"To lobby for support from public and private institutions for intensive development projects and provision of sustainable portable water and basic amenities.",
		},
		{
			title: "Become A Volunteer",
			image: "assets/img/causes/c3.png",
			text:
				"To enhance the welfare of orphans and needy children through appropriate nutrition, health care and education as well as counseling.",
		},
	];

	ngOnInit(): void {}
}
