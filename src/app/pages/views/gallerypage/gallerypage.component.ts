import { Component, OnInit } from '@angular/core';
import { Meta, Title } from "@angular/platform-browser";

@Component({
	selector: "app-gallerypage",
	templateUrl: "./gallerypage.component.html",
	styleUrls: ["./gallerypage.component.css"],
})
export class GallerypageComponent implements OnInit {
	constructor(private titleService: Title, private meta: Meta) {
		this.titleService.setTitle("Our Gallery | Grace To Grace Charity Foundation");
		this.meta.updateTag({
			name: "description",
			content: "",
		});
		this.meta.updateTag({ name: "keywords", content: "" });
		this.meta.updateTag({ name: "author", content: "RMQ Ceative" });
	}

	galleryBanner = {
		mainText: "Gallery",
		subText: "Browse through our pictures & videos",
	};

	ngOnInit(): void {}
}
