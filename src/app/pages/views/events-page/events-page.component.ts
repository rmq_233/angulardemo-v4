import { Component, OnInit } from "@angular/core";
import { Meta, Title } from "@angular/platform-browser";

@Component({
  selector: "app-events-page",
  templateUrl: "./events-page.component.html",
  styleUrls: ["./events-page.component.css"],
})
export class EventsPageComponent implements OnInit {
  constructor(private titleService: Title, private meta: Meta) {
    this.titleService.setTitle("Events | Grace To Grace Charity Foundation");
    this.meta.updateTag({
      name: "description",
      content: "",
    });
    this.meta.updateTag({ name: "keywords", content: "" });
    this.meta.updateTag({ name: "author", content: "RMQ Ceative" });
  }

  eventsBanner = {
    mainText: "Events",
    subText: "Up Coming and Past Events",
  };

  ngOnInit(): void {}
}
