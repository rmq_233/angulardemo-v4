import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-event-info",
  templateUrl: "./event-info.component.html",
  styleUrls: ["./event-info.component.css"],
})
export class EventInfoComponent implements OnInit {
  constructor() {}

  EventDetails = {
    title: "Event Title",
    introText:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsa deserunt vel enim possimus adipisci? Maxime officia doloremque fugit.",
    mainText:
      "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsa deserunt vel enim possimus adipisci? Maxime officia doloremque fugit. Delectus cupiditate animi aut perferendis ipsum ipsam eligendi dicta sint porro blanditiis. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsa deserunt vel enim possimus adipisci? Maxime officia doloremque fugit. Delectus cupiditate animi aut perferendis ipsum ipsam eligendi dicta sint porro blanditiis.",
    eventDate: "Saturday, 15th September, 2020",
    eventVenue: "Azumah Nelson Sports Complex",
    eventLocation: "Accra, Ghana",
  };

  ngOnInit(): void {}
}
