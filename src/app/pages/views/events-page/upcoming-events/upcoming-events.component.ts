import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-upcoming-events",
  templateUrl: "./upcoming-events.component.html",
  styleUrls: ["./upcoming-events.component.css"],
})
export class UpcomingEventsComponent implements OnInit {
  constructor() {}

  event_1 = {
    title: "Event One",
    subtitle:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi itaque labore, eveniet repudiandae.",
  };
  event_2 = {
    title: "Event Two",
    subtitle:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi itaque labore, eveniet repudiandae.",
  };
  event_3 = {
    title: "Event Three",
    subtitle:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi itaque labore, eveniet repudiandae.",
  };
  event_4 = {
    title: "Event Four",
    subtitle:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi itaque labore, eveniet repudiandae.",
  };

  ngOnInit(): void {}
}
