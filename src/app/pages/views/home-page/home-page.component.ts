import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { SeoService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
})
export class HomePageComponent implements OnInit {
  homeBanner: any;
  seoData: any = {
    title: 'Home | Grace To Grace Charity Foundation',
    description:
      // tslint:disable-next-line: max-line-length
      'The NGO started with 11 Girls and 8 Boys with 2 female teachers,My wife and My self being part of the teaching Staffs. Currently the NGO has over 240 plus Children, 16 teaching staffs and 3 non teaching staffs.',
    keywords: '',
    author: 'RMQ Ceative',
  };

  constructor(
    private seo: SeoService,
    public data: DataService
  ) {
    this.homeBanner = this.data.data.homeBanner;
    this.seo.setSeo(this.seoData);
  }

  ngOnInit(): void {}
}
