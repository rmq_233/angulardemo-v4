import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-blog-widget",
  templateUrl: "./blog-widget.component.html",
  styleUrls: ["./blog-widget.component.css"],
})
export class BlogWidgetComponent implements OnInit {
  constructor() {}

  popularPost_1 = {
    Title: "Post One",
    Time: "54 minutes ago",
    Link: "/blog/blog_info",
  };
  popularPost_2 = {
    Title: "Post Two",
    Time: "2 hours ago",
    Link: "/blog/blog_info",
  };
  popularPost_3 = {
    Title: "Post Three",
    Time: "Friday, April 20th 2020",
    Link: "/blog/blog_info",
  };

  postCategory_1 = {
    Name: "Education",
    Number: "23",
  };
  postCategory_2 = {
    Name: "Lifestyle",
    Number: "7",
  };
  postCategory_3 = {
    Name: "Science",
    Number: "2",
  };
  postCategory_4 = {
    Name: "Humanitarian",
    Number: "10",
  };

  ngOnInit(): void {}
}
