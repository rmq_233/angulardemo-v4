import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-new-story",
  templateUrl: "./new-story.component.html",
  styleUrls: ["./new-story.component.css"],
})
export class NewStoryComponent implements OnInit {
  constructor() {}

  Section = {
    Title: "Our Latest Stories",
    Subtitle: "Recent news & activities within / about the organization",
  };

  newStory_1 = {
    Title: "Year Of Return",
    Subtitle:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi itaque labore, eveniet repudiandae.",
    Date: "December 1st, 2019",
    Category: "Travel",
    Link: "/blog/blog_info",
  };

  newStory_2 = {
    Title: "Musical Chairs",
    Subtitle:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi itaque labore, eveniet repudiandae.",
    Date: "June 25th, 2019",
    Category: "Education",
    Link: "/blog/blog_info",
  };

  newStory_3 = {
    Title: "School Feeding Program 2020",
    Subtitle:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi itaque labore, eveniet repudiandae.",
    Date: "February 8st, 2020",
    Category: "Education",
    Link: "/blog/blog_info",
  };

  ngOnInit(): void {}
}
