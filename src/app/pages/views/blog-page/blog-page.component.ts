import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: "app-blog-page",
  templateUrl: "./blog-page.component.html",
  styleUrls: ["./blog-page.component.css"],
})
export class BlogPageComponent implements OnInit {

  constructor (
    private titleService: Title,
    private meta: Meta
    ) {
    this.titleService.setTitle("Our Blog & News | Grace To Grace Charity Foundation");
    this.meta.updateTag({
      name: "description",
      content: "",
    });
    this.meta.updateTag({ name: "keywords", content: "" });
    this.meta.updateTag({ name: "author", content: "RMQ Ceative" });
  }

  blogBanner = {
    mainText : 'Blog',
    subText : 'Upcoming & featured news with our blog.'
  }

  ngOnInit(): void {}
}
