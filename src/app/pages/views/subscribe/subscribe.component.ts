import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit {

  constructor() { }

  subscribeText = {
    title: "Do you have a question?",
    button: "Subscribe",
  }

  ngOnInit(): void {
  }

}
