import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
	selector: "app-footer",
	templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.css"],
  providers: [DatePipe]
})
export class FooterComponent implements OnInit {
  currentDate = Date.now();

	constructor() {}

	footerCol = {
		heading: "Our Mission",
		text:
			"We cater for almost 255 orphans and needy but brilliant children at the ages of 2 - 15 years, at Gomoa Fetteh Kakraba, in the Central Region of Ghana - West Africa. The vision and mission of Grace To Grace Charity Foundation is to provide a unique educational support and aid to children in less privileged communities.",
	};

	footerLinks: any[] = [
		{
			name: "Home",
			link: "",
		},
		{
			name: "About Us",
			link: "/about_us",
		},
		{
			name: "Causes",
			link: "/causes",
		},
		{
			name: "Gallery",
			link: "/gallery",
		},
		{
			name: "Contact Us",
			link: "/contact_us",
		},
	];

	galleryList: any[] = [
		{
			image: "assets/img/gallery/g1.jpg",
		},
		{
			image: "assets/img/gallery/g2.jpg",
		},
		{
			image: "assets/img/gallery/g3.jpg",
		},
		{
			image: "assets/img/gallery/g4.jpg",
		},
	];

	quickContact: any[] = [
		{
			title: "High Tension",
			text: "Gomoa Fetteh Kakraba, Central Region - Ghana",
			icon: "fa-location-arrow",
		},
		{
			title: "Speak with us anytime.",
			text: "Mon - Sun | (+233) 54 769 6997 / (+233) 54 297 3008",
			icon: "fa-phone",
		},
		{
			title: "Email Us",
			text: "info@gracefoundus.org",
			icon: "fa-envelope",
		},
	];

	ngOnInit(): void {}
}
