import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
	selector: "app-header",
	templateUrl: "./header.component.html",
	styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit {
	navLinks: any[] = [
		{
			name: "Home",
			link: "/home",
		},
		{
			name: "About Us",
			link: "/about_us",
		},
		{
			name: "Causes",
			link: "/causes",
		},
		{
			name: "Gallery",
			link: "/gallery",
		},
		{
			name: "Contact Us",
			link: "/contact_us",
		},
	];

	constructor() {}

	ngOnInit() {
		"use strict";
		var nav_offset_top = $(".header_area").height() + 50;
		function navbarFixed() {
			if ($(".header_area").length) {
				$(window).scroll(function () {
					var scroll = $(window).scrollTop();
					if (scroll >= nav_offset_top) {
						$(".header_area").addClass("navbar_fixed");
					} else {
						$(".header_area").removeClass("navbar_fixed");
					}
				});
			}
		}
		navbarFixed();
	}
}
