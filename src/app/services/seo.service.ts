import { Meta, Title } from '@angular/platform-browser';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: "root",
})
export class SeoService {

  constructor(
    private titleService: Title,
    private meta: Meta
  ) { }

  setSeo(data:any) {
    this.titleService.setTitle(data.title);
    this.meta.updateTag({
      name: "description",
      content: data.description || "",
    });
    this.meta.updateTag({ name: "keywords", content: data.keyboards });
    this.meta.updateTag({ name: "author", content: data.author });
  }
}
