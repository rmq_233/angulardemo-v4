import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class DataService {
  public data: any = {
    homeBanner: {
      title1: "Give A Hand",
      title2: "To Make The World A Better Place",
      subtitle:
        "“You want to be the pebble in the pond that creates the ripple for change.” Tim Cook.",
      buttonText1: "Causes",
      buttonLink1: "/causes"
    },
  };

  constructor() {}
}
